'use strict';

// requirements
const express = require('express');

// constants
const PORT = process.env.PORT || 8080;

// main express program
const app = express();

// configurations
app.use(express.json());

// routes
// health check
app.get('/status', (req, res) => { res.status(200).end(); });
app.head('/status', (req, res) => { res.status(200).end(); });

// Send
app.get('/', (req, res) => {
    var balance = 1000;
    var { balance, transfered } = transfer(balance, req.query.amount);
    if (balance !== undefined || transfered !== undefined) {
        res.status(200).end('Successfully transfered: ' + transfered + '. Your balance: ' + balance);
    } else {
        res.status(400).end('Insufficient funds. Your balance: ' + balance);
    }
});

// var lock = 0;

// Transfer amount service
var transfer = (balance, amount) => {
    var transfered = 0;
    const drawdown = parseInt(amount)
    if (drawdown > 0 && drawdown <= balance) {
        balance = balance - drawdown;
        transfered = transfered + parseInt(drawdown);
        return { balance, transfered };
    } else
        return { undefined, undefined };
};

// Fix to avoid EADDRINUSE during test
if (!module.parent) {
    // HTTP listener
    app.listen(PORT, err => {
        if (err) {
            console.log(err);
            process.exit(1);
        }
        console.log('Server is listening on port: '.concat(PORT));
    });
}
// CTRL+c to come to action
process.on('SIGINT', function() {
    process.exit();
});

module.exports = { app, transfer };
